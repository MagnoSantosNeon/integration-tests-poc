﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using poc.integration_tests.domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace poc_integration_tests.infra
{
    public class ExampleHealthCheck : IHealthCheck
    {
        private readonly IAnyService _anyService;

        public ExampleHealthCheck(IAnyService anyService)
        {
            _anyService = anyService;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(
            HealthCheckContext context,
            CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await _anyService.AnyMethod();
                
                if (result is true)
                    return HealthCheckResult.Healthy("A healthy result.");

                return HealthCheckResult.Unhealthy("An unhealthy result.");
            }
            catch (Exception ex)
            {
                return HealthCheckResult.Unhealthy(
                    description: "An unhealthy result.", 
                    exception: ex
                );
            }
        }
    }
}