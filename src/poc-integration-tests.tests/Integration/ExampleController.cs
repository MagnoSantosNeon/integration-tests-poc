﻿using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using poc.integration_tests;
using System.Net;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Xunit;

namespace poc_integration_tests.tests.Integration
{
    public class ExampleController : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;

        public ExampleController(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Theory]
        [InlineData("api/Example")]
        public async Task Add_EndpointsReturnSuccess(string url)
        {
            // Arrange
            var client = _factory.CreateClient();

            // Act
            var response = await client.PostAsJsonAsync(url, new
            {
                name = "Foo",
                lastName = "Bar",
                email = "foo.bar@test.com.br",
                cpf = "12345678900"
            });

            // Assert
            response.EnsureSuccessStatusCode(); // Status Code 200-299
            response.StatusCode.Should().Be(HttpStatusCode.Created);
        }
    }
}