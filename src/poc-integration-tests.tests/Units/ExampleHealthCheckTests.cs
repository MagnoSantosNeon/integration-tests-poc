using FluentAssertions;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Moq;
using poc.integration_tests.domain;
using poc_integration_tests.infra;
using System;
using System.Threading.Tasks;
using Xunit;

namespace poc_integration_tests.tests
{
    public class ExampleHealthCheckTests
    {
        private readonly Mock<IAnyService> _anyServiceMock;

        public ExampleHealthCheckTests()
        {
            _anyServiceMock = new Mock<IAnyService>();
        }

        [Fact]
        public async Task CallMethodVerificationHealthExample_ShouldBeStatusHealthy()
        {
            var context = new HealthCheckContext();
            _anyServiceMock
               .Setup(mock => mock.AnyMethod())
               .ReturnsAsync(true);
            var health = Instanteate();

            var resposta = await health.CheckHealthAsync(context, default);

            resposta.Should().NotBeNull();
            resposta.Status.Should().Be(HealthStatus.Healthy);
        }

        [Fact]
        public async Task CallMethodVerificationHealthExample_ShouldBeStatusUnHealthy()
        {
            var context = new HealthCheckContext();
            _anyServiceMock
                .Setup(mock => mock.AnyMethod())
                .ReturnsAsync(false);
            var health = Instanteate();

            var resposta = await health.CheckHealthAsync(default, default);

            resposta.Should().NotBeNull();
            resposta.Status.Should().Be(HealthStatus.Unhealthy);
        }

        [Fact]
        public void CallMethodVerificationHealthExample_ShouldBeStatusUnHealthyThrowsExceptionInProcess()
        {
            var context = new HealthCheckContext();
            _anyServiceMock
                .Setup(mock => mock.AnyMethod())
                .ThrowsAsync(new Exception());
            var health = Instanteate();

            Assert.ThrowsAsync<Exception>(async () => await health.CheckHealthAsync(default, default));
        }

        private ExampleHealthCheck Instanteate()
        {
            return new ExampleHealthCheck(_anyServiceMock.Object);
        }
    }
}