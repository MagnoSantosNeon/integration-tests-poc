﻿using poc.integration_tests.domain;
using System;
using Xunit;

namespace poc_integration_tests.tests
{
    public class AnyServiceTests
    {
        [Fact]
        public void AnyMethod_ThrowsAsyncNotImplementedException()
        {
            var service = Instantiate();

            Assert.ThrowsAsync<NotImplementedException>(async () => await service.AnyMethod());
        }

        private AnyService Instantiate()
        {
            return new AnyService();
        }
    }
}