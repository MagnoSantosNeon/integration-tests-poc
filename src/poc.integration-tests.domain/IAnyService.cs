﻿using System.Threading.Tasks;

namespace poc.integration_tests.domain
{
    public interface IAnyService
    {
        Task<bool> AnyMethod();
    }
}