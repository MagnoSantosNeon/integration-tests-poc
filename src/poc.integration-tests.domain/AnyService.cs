﻿using System.Threading.Tasks;

namespace poc.integration_tests.domain
{
    public class AnyService : IAnyService
    {
        public Task<bool> AnyMethod()
        {
            return Task.FromResult(true);
        }
    }
}