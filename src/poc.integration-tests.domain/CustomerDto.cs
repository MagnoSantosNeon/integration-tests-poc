﻿using System.Text.Json.Serialization;

namespace poc.integration_tests.domain
{
    public class CustomerDto
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("lastName")]
        public string LastName { get; set; }

        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonPropertyName("cpf")]
        public string Cpf { get; set; }
    }
}