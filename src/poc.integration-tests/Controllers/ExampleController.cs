﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using poc.integration_tests.domain;
using System.Threading.Tasks;

namespace poc.integration_tests.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExampleController : ControllerBase
    {
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Add([FromBody] CustomerDto customerDto)
        {
            return CreatedAtAction(nameof(Add), customerDto);
        }
    }
}