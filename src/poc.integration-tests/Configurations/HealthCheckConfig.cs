﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using poc_integration_tests.infra;
using System.Diagnostics.CodeAnalysis;

namespace poc.integration_tests.Configurations
{
    [ExcludeFromCodeCoverage]
    public static class HealthCheckConfig
    {
        public static void InjectHealthCheckServicesExtensions(this IServiceCollection services)
        {
            services
                .AddHealthChecks()
                .AddCheck<ExampleHealthCheck>(
                    "example_health_check",
                    failureStatus: HealthStatus.Degraded,
                    tags: new[] { "example" });
        }
    }
}