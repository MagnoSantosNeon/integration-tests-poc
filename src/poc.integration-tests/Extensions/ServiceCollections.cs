﻿using Microsoft.Extensions.DependencyInjection;
using poc.integration_tests.domain;
using System.Diagnostics.CodeAnalysis;

namespace poc.integration_tests.Extensions
{
    [ExcludeFromCodeCoverage]
    public static class ServiceCollections
    {
        public static void InjectDomain(this IServiceCollection services)
        {
            services
                .AddSingleton<IAnyService, AnyService>();
        }
    }
}