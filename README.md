# Repositório Integration Tests

## Features
- Testes de integração utilizando Microsoft.AspNetCore.Mvc.Testing .NET Core 5.0

## Utilização

Os testes de integração, o projeto testado é frequentemente chamado de sistema de teste, ou "SUT". 

- Um projeto de testes é usado para conter e executar os testes. O projeto possui uma referência para o SUT. 

- O projeto de teste cria um host web de teste para o SUT e usa um cliente de servidor de teste para lidar com as solicitações e respostas com o SUT. 

- Um executor de teste é usado para executar os testes e relatar os resultados gerados. 

Os testes de ingração seguem uma sequência de eventos que incluem as etapas de teste de Arrange, Act e Assert usuais: 

- O host web do SUT é configurado. 

- Um cliente de servidor de teste é criado para enviar solicitações ao aplicativo. 

- A etapa de arrange é executada: o aplicativo de teste prepara uma solicitação.

- A etapa de act é executada: o cliente envia a solicitação e recebe a resposta. 

- A etapa de assert é executada: resposta real é validada com uma passagem ou falha com base em uma resposta esperada. 

- O processo continua até que os testes sejam exetudas. 

- Os resultados de teste são relatados. 

Componentes de infraestrutura, como o host da Web de teste e o servidor de teste na memória ( [TestServer](https://docs.microsoft.com/pt-br/dotnet/api/microsoft.aspnetcore.testhost.testserver?view=aspnetcore-5.0) ), são fornecidos ou gerenciados pelo pacote [Microsoft.AspNetCore.Mvc.Testing](https://www.nuget.org/packages/Microsoft.AspNetCore.Mvc.Testing) O uso desse pacote simplifica a criação e a execução de testes.

O Microsoft.AspNetCore.Mvc.Testing lida com as seguintes tarefas:

- Copia o arquivo de dependências ( .deps ) do SUT para o diretório do projeto de teste bin .
- Define a raiz do conteúdo como a raiz do projeto do SUT para que arquivos estáticos e páginas/exibições sejam encontrados quando os testes forem executados.
- Fornece a classe WebApplicationFactory para simplificar a inicialização do SUT com TestServer .

## Exemplo - Health Check Controller
- Definição da classe contendo o health check: 

```cs
public class ExampleHealthCheck : IHealthCheck
{
    private readonly IAnyService _anyService;

    public ExampleHealthCheck(IAnyService anyService)
    {
        _anyService = anyService;
    }

    public async Task<HealthCheckResult> CheckHealthAsync(
        HealthCheckContext context,
        CancellationToken cancellationToken = default)
    {
        try
        {
            var result = await _anyService.AnyMethod();
                
            if (result is true)
                return HealthCheckResult.Healthy("A healthy result.");

            return HealthCheckResult.Unhealthy("An unhealthy result.");
        }
        catch (Exception ex)
        {
            return HealthCheckResult.Unhealthy(
                description: "An unhealthy result.", 
                exception: ex
            );
        }
    }
}
```

- Classe de teste do health check: 
```cs
public class ExampleHealthCheckTests : IClassFixture<WebApplicationFactory<Startup>>
{
    private readonly WebApplicationFactory<Startup> _factory;

    public ExampleHealthCheckTests(WebApplicationFactory<Startup> factory)
    {
        _factory = factory;
    }

    [Theory]
    [InlineData("/health")]
    public async Task Get_EndpointsReturnSuccess(string url)
    {
        // Arrange
        var client = _factory.CreateClient();

        // Act
        var response = await client.GetAsync(url);

        // Assert
        response.EnsureSuccessStatusCode(); // Status Code 200-299
    }
}
```

É possível personalizar a classe de testes com a adoção do ```WebApplicationFactory<TEntryPoint>```onde é possível criar um TestServer para os testes. ```TEntryPoint```e classe de ponto de entrada do SUT, geralmente a classe ```Startup```.
A configuração do host web pode ser criado de forma independente das classes de testes herdando ```WebApplicationFactory``` para criar uma ou mais fábricas personalizadas: 
```cs
public class CustomWebApplicationFactory<TStartup>
    : WebApplicationFactory<TStartup> where TStartup: class
{
    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        builder.ConfigureServices(services =>
        {
            var descriptor = services.SingleOrDefault(
                d => d.ServiceType ==
                    typeof(DbContextOptions<ApplicationDbContext>));

            services.Remove(descriptor);

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseInMemoryDatabase("InMemoryDbForTesting");
            });

            var sp = services.BuildServiceProvider();

            using (var scope = sp.CreateScope())
            {
                var scopedServices = scope.ServiceProvider;
                var db = scopedServices.GetRequiredService<ApplicationDbContext>();
                var logger = scopedServices
                    .GetRequiredService<ILogger<CustomWebApplicationFactory<TStartup>>>();

                db.Database.EnsureCreated();

                try
                {
                    Utilities.InitializeDbForTests(db);
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "An error occurred seeding the " +
                        "database with test messages. Error: {Message}", ex.Message);
                }
            }
        });
    }
}
```

Projeto com outros exemplos de testes: https://github.com/dotnet/AspNetCore.Docs/tree/main/aspnetcore/test/integration-tests/samples/3.x/IntegrationTestsSample/tests/RazorPagesProject.Tests

## License

MIT